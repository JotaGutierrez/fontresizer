
(function( $ ) {

	var level = 0;

	var defaults = {
		'highLimit': 2,
		'lowLimit': 0,
	};

	var domain = '';
	
	var resize = function(action){	

		if(level >= defaults.highLimit && action === 'increase')
			return;
		
		if(level <= defaults.lowLimit && action === 'decrease')
			return;

		if(action === 'increase') level++;
		if(action === 'decrease') level--;

		$.cookie("fontResizer", level, {
							expires : 10,
							domain  : domain,
							path	: '/'
				});

		$('*').each(function(index){

			ele = $(this);
			fs = parseInt(ele.css('font-size').split('px')[0]);

			variation = 1;

			if(action === 'increase')
				fs += variation

			else if(action === 'decrease')
				fs -= variation;

			ele.css( {'font-size':fs + 'px'} );
		});
	}
 
    $.fn.fontResizer = function() {
 		
 		ele = $(this);

 		ele.html('');

 		css ={
 			'font-weight':'bold',
 			'display': 'inline-block',
 			'color': '#000',
 			'margin-right': '4px'
 		};

 		ele.append($('<span />', {
 			id: 'fontResizer-decrease',
 			html: '-A',
 			click: function(){resize('decrease')},
 		}));

 		$('#fontResizer-decrease').addClass('fontResizer');
 		$('#fontResizer-decrease').css($.extend({'font-size':'12px'}, css));

 		ele.append($('<span />', {
 			id: 'fontResizer-increase',
 			html: 'A+',
 			click: function(){ resize('increase') }
 		}));

 		$('#fontResizer-increase').addClass('fontResizer');
 		$('#fontResizer-increase').css($.extend({'font-size':'16px'}, css));

 		$('.fontResizer:hover').css( {'cursor': 'pointer'} );
 		
 		ini_level = $.cookie('fontResizer');
 		
 		if(!isNaN(ini_level)){
	 		if(ini_level != 0){
	 			if(ini_level < 0){
	 				for(x = ini_level ; x < 0 ; x++){
	 					resize('decrease');
	 				}
	 			}
	 			if (ini_level >0 ) {
	 				for(x = 0 ; x < ini_level ; x++){
	 					resize('increase'); 				
	 				}
	 			}
	 		}
	 	}

 		return (ele);
    }; 


}( jQuery ));
